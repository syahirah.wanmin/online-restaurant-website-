<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8">
	   <link rel="icon" href="image/logo3.jpg">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Food Qsystem</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="style/home.css" rel="stylesheet" type="text/css">
    <?php include ('process.php'); ?>
    

	<script>
    
     function validate(){
    
            var a = document.getElementById("pass").value;
            var b = document.getElementById("rpass").value;
            if (a!=b) {
                alert("Passwords do no match");
                return false;
            }
      }	
      
    </script>  
	<style>
		.navbar-brand {
		  background: url(image/logo3.jpg) center / contain no-repeat;
		  width: 230px;
		  transform: scale(1.8);
		}

	</style>		
  </head>
  <body >
    
	<div class="container">
    	
        <div class="koko"></div>	
            
		<div class="navbar navbar-default navbar-inverse " role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <span class="navbar-brand hidden-xs hidden-sm"></span> 
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                  <li class="active"><a href="#" id="home">Home</a></li>
                  <li><a href="#" id="menu">Best Menu</a></li>
                  <li><a href="#" id="order">Make Order!</a></li>
                  <li><a href="#" id="about">About Us</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
			   <?php 
               
                    if(isset($_SESSION['username'])){
                        echo '<li>
								  <p class="navbar-text">
								  	<span class="glyphicon glyphicon-user" style="color:white;"></span>
								  	<b style="color:white;">'.$_SESSION['username'].'</b> ( <a href="logout.php"> Log out </a> )
								  </p>
							  </li>';
                    }else{
                        echo '
                        <li><a href="#" id="signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#" id="signin"><span class="glyphicon glyphicon-log-in"></span> Sign in</a></li>
                        ';
                    }
                    
                ?>
              </ul>
            </div>
        </div> 
        <div id="contain"></div>
        
    </div><!-- container -->
    
    <!-- SIGN UP -->
<!--	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
        
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title" id="modal-register-label">Register now !!</h3>
            <p>Fill in the form below to get instant access:</p>
          </div>
          
          <div class="modal-body">
              
            <form role="form" action="" method="post" class="registration-form" onSubmit="return validate();" autocomplete="off">
                <div class="form-group">
                    <label class="sr-only" for="form-usrnme">Username</label>
                    <input type="text" name="reg_usrnme" placeholder="Username..." class="form-control" id="usrnme">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-fullname">Full name</label>
                    <input type="text" name="reg_fnme" placeholder="Full name..." class="form-control" id="fname">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-email">Email</label>
                    <input type="email" name="reg_email" placeholder="Email..." class="form-email form-control" id="email">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-phone">Phone</label>
                    <input type="text" name="reg_phone" placeholder="Phone..." class="form-phone form-control" id="phone">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-pass">Password</label>
                    <input type="password" name="reg_pass" placeholder="Password..." class="form-password form-control" id="pass">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-rpass">Retype Password</label>
                    <input type="password" name="reg_rpass" placeholder="Retype Password..." class="form-password form-control" id="rpass">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-about-yourself">Address..</label>
                    <textarea name="address" placeholder="Your current address..." class="form-control" id=""></textarea>
                </div>
                
                <input type="submit" class="btn btn-danger btn-block" value="Register" name="register"/>
            </form>
            
          </div>
          
        </div>
      </div>
    </div> -->
    <!-- LOG IN -->
<!--	<div class="modal fade" id="ModalSaya" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm ">
        <div class="modal-content">
        
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
            </button>
            <h3 class="modal-title" id="modal-register-label">Log In</h3>
            <p>Log in first to make order:</p>
          </div>
          
          <div class="modal-body">
            <form role="form" action="" method="post" class="registration-form">
                <div class="form-group">
                    <label class="sr-only">Username</label>
                    <input type="text" name="login_usr" placeholder="Username..." class="form-control" id="">
                </div>
                <div class="form-group">
                    <label class="sr-only">Password</label>
                    <input type="password" name="login_pass" placeholder="Password..." class="form-password form-control" id="">
                </div>
                <input type="submit" class="btn btn-danger btn-block" value="Login" name="login"/>

            </form>
          </div>
          
        </div>
      </div>
    </div> -->
  
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <script>
    
	  $(document).ready(function(){
		  
			  $("#contain").load("home.php").hide().fadeIn("slow");
			  
			  $('li > a').click(function() {
				  $('li').removeClass();
				  $(this).parent().addClass('active');
			  });
		  			  
			  $("#home").click(function(e){
				  $("#contain").load("home.php").hide().fadeIn("slow");
			  });
		  			  			  
			  $("#menu").click(function(){
				  $("#contain").load("special_menu.php").hide().fadeIn("slow");
			  });
			  
			  $("#order").click(function(){
				  setTimeout(' window.location.href = "customer.php"; ',100);
			  });
			  
			  $("#about").click(function(){
				  $("#contain").load("about.php");
			  });
			  
			  $("#signin").click(function(){
				  $("#contain").load("sign_in.php");
			  });
			  
			  $("#signup").click(function(){
				  $("#contain").load("sign_up.php");
			  });
			  
	  });
	  
    
    </script>
  </body>
</html>