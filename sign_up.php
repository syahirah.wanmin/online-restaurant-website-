<link href="style/hidden_scroll.css" rel="stylesheet" type="text/css">
<script src="js/jquery.min.js"></script>


	<style>
      ul#stepForm, ul#stepForm li {
        margin: 0;
        padding: 0;
      }
      ul#stepForm li {
        list-style: none outside none;
      } 
      label{margin-top: 10px;}
      .help-inline-error{color:red;}
	  footer {
		  position: fixed;
		  height: 100px;
		  bottom: 0;
		  width: 100%;
	  }
	  .row{
		  background-color:maroon;
	  }
	  .panel{
		  margin-top:30px;
	  }
	  .clearfix{
		  height: 10px;
		  clear: both;
	  }
	  .username{ display:inline-block;}
    </style>
<div class="row"><div class="col-md-12 text-center bg-info" id="reginfo"></div></div>
<div class="row">
<div class="col-md-8 col-md-offset-2">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Complete this form in 3 steps!</h3>
    </div>
    
    <div class="panel-body">
        <form name="basicform" id="basicform" method="post" >
        
            <div id="sf1" class="frm">
                  <fieldset>
                    <legend>Step 1 of 3</legend>
        
                    <div class="form-group">
                          <label class="col-lg-2 control-label" for="fullname">FullName: </label>
                          <div class="col-lg-6">
                            <input type="text" placeholder="Your Name" id="uname" name="uname" class="form-control" autocomplete="off">
                          </div>
                    </div><div class="clearfix"></div>
                    
                    <div class="form-group">
                          <label class="col-lg-2 control-label" for="usename">Username: </label>
                          <div class="col-lg-6">
                            <input type="text" placeholder="Enter username" id="usrnme" name="usrnme" class="form-control" onKeyUp="checkUser(this.value)" autocomplete="on"/>
                          </div>
                          <div class="col-lg-4" id="msg"></div>
                    </div><div class="clearfix"></div>
                    
                    <div class="form-group">
                          <label class="col-lg-2 control-label" for="uname">Address: </label>
                          <div class="col-lg-6">
                            <textarea rows="5" cols="30" placeholder="enter optional message" id="address" name="address" class="form-control" autocomplete="on"> </textarea>
                      </div>
                    </div>
                      
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
        
        
                    <div class="form-group">
                      <div class="col-lg-10 col-lg-offset-2">
                        <button class="btn btn-primary open1" type="button" id="mukasatu">Next <span class="glyphicon glyphicon-arrow-right"></span></button> 
                      </div>
                    </div>
        
                  </fieldset>
            </div>

            <div id="sf2" class="frm" style="display: none;">
                  <fieldset>
                    <legend>Step 2 of 3</legend>
        
        
                    <div class="form-group">
                      <label class="col-lg-2 control-label" for="uemail">Email: </label>
                      <div class="col-lg-6">
                        <input type="text" placeholder="Your Email" id="uemail" name="uemail" class="form-control" autocomplete="on">
                      </div>
                    </div><div class="clearfix"></div>
                    
                    <div class="form-group">
                      <label class="col-lg-2 control-label" for="phone">Phone Number: </label>
                      <div class="col-lg-6">
                        <input type="text" placeholder="Your Phone number" id="phone" name="phone" class="form-control" autocomplete="off">
                      </div>
                    </div><div class="clearfix"></div>
        
                    <div class="form-group">
                      <div class="col-lg-10 col-lg-offset-2">
                        <button class="btn btn-warning back2" type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button> 
                        <button class="btn btn-primary open2" type="button">Next <span class="glyphicon glyphicon-arrow-right"></span></button> 
                      </div>
                    </div>
        
                  </fieldset>
            </div>

            <div id="sf3" class="frm" style="display: none;">
                  <fieldset>
                    <legend>Step 3 of 3</legend>
        
                    <div class="form-group">
                      <label class="col-lg-2 control-label" for="upass1">Password: </label>
                      <div class="col-lg-6">
                        <input type="password" placeholder="Your Password" id="upass1" name="upass1" class="form-control" autocomplete="off">
                      </div>
                    </div><div class="clearfix" style="height: 10px;clear: both;"></div>
        
                    <div class="form-group">
                      <label class="col-lg-2 control-label" for="upass1">Confirm Password: </label>
                      <div class="col-lg-6">
                        <input type="password" placeholder="Confirm Password" id="upass2" name="upass2" class="form-control" autocomplete="off">
                      </div>
                    </div><div class="clearfix" style="height: 10px;clear: both;"></div>
        
                    <div class="form-group">
                      <div class="col-lg-10 col-lg-offset-2">
                            <button class="btn btn-warning back3" type="button"><span class="glyphicon glyphicon-arrow-left"></span> Back</button> 
                            <button class="btn btn-primary open3" id="register" type="button">Submit </button> 
                            <img src="image/loader.gif" height="70" width="100"  alt="" id="loader" style="display:none;">
                      </div>
                    </div>
        
                  </fieldset>
            </div>
      </form>
    </div>
  </div>
  
</div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script>
  
  jQuery().ready(function() {

    // validate form on keyup and submit
    var v = jQuery("#basicform").validate({
      rules: {
		  uname: {
			required: true,
			maxlength: 100,
		  },
		  usrnme:{
			required: true,
		  },
		  uemail: {
			required: true,
			minlength: 2,
			email: true,
			maxlength: 100,
		  },
		  upass1: {
			required: true,
			minlength: 6,
			maxlength: 15,
		  },
		  upass2: {
			required: true,
			minlength: 6,
			equalTo: "#upass1",
		  },
		  phone: {
			  required: true,
			  number : true,
			  minlength :10,
			  maxlength :11,
		  },
		  address: {
			  required: true,
		  },
  
      },
	   messages: {

  	  },
      errorElement: "span",
      errorClass: "help-inline-error",
    });
	

    $(".open1").click(function() {
      if (v.form()) {
        $(".frm").hide("fast");
        $("#sf2").show("slow");
      }
    });

    $(".open2").click(function() {
      if (v.form()) {
        $(".frm").hide("fast");
        $("#sf3").show("slow");
      }
    });
    
    /*$(".open3").click(function() {
      if (v.form()) {
        $("#loader").show();
         setTimeout(function(){
           $("#basicform").html('<h2>Thanks for your time.</h2>');
         }, 1000);
        return false;
      }
    });*/
    
    $(".back2").click(function() {
      $(".frm").hide("fast");
      $("#sf1").show("slow");
    });

    $(".back3").click(function() {
      $(".frm").hide("fast");
      $("#sf2").show("slow");
    });

  });
  
  $(document).ready(function() {
	  
	  $("#register").click(function(){
		  var nme = $("#uname").val();
		  var usr = $("#usrnme").val();	
		  var eml = $("#uemail").val();	
		  var add = $("#address").val();	
		  var phn = $("#phone").val();	
		  var pss = $("#upass1").val();	
		  
		  var value = "fullname=" + nme + "&username=" + usr + "&email=" + eml + "&address=" + add + "&phone=" + phn + "&password=" + pss  ;
		  
		  $.ajax({
			  method: "POST",
			  url: "sign_up_process.php",
			  data: value,
			  success: function(value){
				  
				  if(value === "Success"){
					$("#loader").show();
					setTimeout(function(){ $("#basicform").html('<h2>Thanks for your time.</h2>'); }, 4000);
				  }else{
					  $("#loader").show();
					  setTimeout(function(){ $("#basicform").html('<h2>Registration fail.please try again.</h2>'); }, 4000);
				  }
			  }
			  
		  });
	  
	  });
  });
  
function checkUser(val){
	
	$.ajax({
		type: "POST",
		url: "duplicate.php",
		data: "username="+val,
		success: function(data){
			if(data != "ada"){
				$("#msg").html('<b style="color:green;">Username can be use!</b>');
				$("#mukasatu").removeClass("disabled");
			}else{
				$("#msg").html('<b class="help-inline-error">Username already exist!</b>');
				$("#mukasatu").addClass("disabled");
			}
		}
	});
}
		
</script>
