<script src="js/jquery.min.js"></script>

<style>
.form-body{
    background:#fff;
    padding:20px;
}
.login-form{
    background:rgba(255,255,255,0.8);
	padding:20px;
	border-top:3px solid#3e4043;
}
.innter-form{
	padding-top:20px;
}
.final-login li{
	width:50%;
}

.nav-tabs {
    border-bottom: none !important;
}

.nav-tabs>li{
	color:#222 !important;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
    color: #fff;
    background-color: #d14d42;
    border: none !important;
    border-bottom-color: transparent;
	border-radius:none !important;
}
.nav-tabs>li>a {
    margin-right: 2px;
    line-height: 1.428571429;
    border: none !important;
    border-radius:none !important;
	text-transform:uppercase;
	font-size:16px;
}

.social-login{
	text-align:center;
	font-size:12px;
}
.social-login p{
	margin:15px 0;
}
.social-login ul{
	margin:0;
	padding:0;
	list-style-type:none;
}
.social-login ul li{
	width:33%;
	float:left;
    clear:fix;
}
.social-login ul li a{
	font-size:13px;
	color:#fff;
	text-decoration:none;
	padding:10px 0;
	display:block;
}
.social-login ul li:nth-child(1) a{
	background:#3b5998;
}
.social-login ul li:nth-child(2) a{
	background:#e74c3d;
}
.social-login ul li:nth-child(3) a{
	background:#3698d9;
}
.sa-innate-form input[type=text], input[type=password], input[type=file], textarea, select, email{
    font-size:13px;
	padding:10px;
	border:1px solid#ccc;
	outline:none;
	width:100%;
	margin:8px 0;
	
}
.sa-innate-form input[type=submit]{
    border:1px solid#e64b3b;
	background:#e64b3b;
	color:#fff;
	padding:10px 25px;
	font-size:14px;
	margin-top:5px;
	}
	.sa-innate-form input[type=submit]:hover{
	border:1px solid#db3b2b;
	background:#db3b2b;
	color:#fff;
	}
	
	.sa-innate-form button{
	border:1px solid#e64b3b;
	background:#e64b3b;
	color:#fff;
	padding:10px 25px;
	font-size:14px;
	margin-top:5px;
	}
	.sa-innate-form button:hover{
	border:1px solid#db3b2b;
	background:#db3b2b;
	color:#fff;
	}
    .sa-innate-form p{
        font-size:13px;
        padding-top:10px;
    }
	.row{
		background-color:maroon;
	}
	.form-body{
		margin-top:30px;
	}
</style>	
<link href="style/hidden_scroll.css" rel="stylesheet" type="text/css">

<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="form-body">
    
        <ul class="nav nav-tabs final-login">
            <li class="active"><a data-toggle="tab" href="#sectionA">Customer</a></li>
            <li><a data-toggle="tab" href="#sectionB">Admin</a></li>
        </ul>
        <div class="text-center" id="message"></div>
        <div class="tab-content">
        
            <div id="sectionA" class="tab-pane fade in active">
                <div class="innter-form">
                    <div class="sa-innate-form">
                        <label>Username or Email address</label>
                        <input type="text" name="username" id="username">
                        <label>Password</label><input type="password" name="password" id="password">
                        <input type="hidden" name="index" id="index"  value="customer"/>
                        <button type="submit" id="login_button">Sign In</button>
                        <a href="">Forgot Password?</a>
                    </div>
                </div>
            </div>
            
            <div id="sectionB" class="tab-pane fade">
                <div class="innter-form">
                    <div class="sa-innate-form" >
                        <label>Username or Email address</label>
                        <input type="text" name="admin_name" id="admin_name">
                        <label>Password</label>
                        <input type="password" name="admin_pass" id="admin_pass">
                        <input type="hidden" name="admin_indx" id="admin_idx"  value="admin"/>
                        <button type="submit" id="login_admin">Sign In</button>
                    </div>
                </div>
            </div>
            
        </div><!-- tab cotent -->
        
    </div>
  </div>
</div>
<script>
	$(document).ready(function() {
		
		$("#login_button").click(function(){
			var user = $("#username").val();	
			var pass = $("#password").val();
			var index = $("#index").val();
			
			var data = "user=" + user + "&pass=" + pass + "&index=" + index;
				
			$.ajax({
				method: "post",
				url: "sign_in_user.php",
				data: data,
				success: function(data){
					if(data == 'Success'){
					  $("#message").html('<img src="image/loader.gif" height="50" weight="50"/><b style="color:green;font-weight:bold;">Successfull, please wait</b>...');
					  setTimeout(' window.location.href = "customer.php"; ',2000);
					}else{
						$("#message").html('<b style="color:red; font-weight:bold;">Invalid login!..please try again.</b>');
					}
				}
			});
		});
		
		$("#login_admin").click(function(){
			
			var user = $("#admin_name").val();	
			var pass = $("#admin_pass").val();
			var index = $("#admin_idx").val();
			
			var admin = "user=" + user + "&pass=" + pass + "&index=" + index;
			
			$.ajax({
				method: "post",
				url: "sign_in_admin.php",
				data: admin,
				success: function(admin){
					if(admin == 'Success'){
					  $("#message").html('<img src="image/loader.gif" height="50" weight="50"/><b style="color:green;font-weight:bold;">Successfull, please wait</b>...');
					  setTimeout(' window.location.href = "admin.php"; ',2000);
					}else{
						$("#message").html('<b style="color:red; font-weight:bold;">Invalid login!..please try again.</b>');
					}
				}
			});
		});

		
		
	});
</script>
