<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Admin panel</title>
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="style/admin.css" rel="stylesheet" type="text/css">
</head>
<style>
footer {
    position: fixed;
    height: 100px;
    bottom: 0;
    width: 75%;
}
</style>
<body>

<div class="container">
	<div class="row">
		<div class="wrapper">
    	    <div class="side-bar">
                <ul>
                    <li class="menu-head">
                        Admin Name <a href="#" class="push_menu"><span class="glyphicon glyphicon-align-justify pull-right"></span></a>
                    </li>
                    <div class="menu">
                        <li>
                            <a href="#" id="overview">Overview <span class="glyphicon glyphicon-signal pull-right"></span></a>
                        </li>
                        <li>
                            <a href="#" id="customer" class="active">Customer Setting<span class="glyphicon glyphicon-book pull-right"></span></a>
                        </li>
                        <li>
                            <a href="#" id="menu">Menu Setting <span class="glyphicon glyphicon-th-list pull-right"></span></a>
                        </li>
                        <li>
                            <a href="#" id="profile">Profile Setting <span class="glyphicon glyphicon-user pull-right"></span></a>
                        </li>
                        <li>
                            <a href="#">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                        </li>
                    </div>
                    
                </ul>
    	    </div>   
            <div class="content">
                    <div class="col-md-12"><h3>Customer Setting</h3><hr></div>
                    <div class="col-md-12">
                      <button class="btn btn-danger btn-md" id="delete" >
                         <span class="glyphicon glyphicon-trash"></span> Delete
                      </button>
                    </div>
                    
                    <div class="col-md-12">
                    
						<?php 
                        
                            include('connect.php');
                            
                            $per_page=6;
                        
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }else {
                                $page=1;
                            }
                        
                            $prev_page = $page-1;
                            $next_page = $page+1;
                            $start_from = ($page-1) * $per_page;
                            
                            $total_records = $db_handle->numRows("SELECT * FROM customer");	
                            $total_pages = ceil($total_records / $per_page);
                            
                            $tableData = $db_handle->runQuery("SELECT * FROM customer LIMIT ".$start_from.", ".$per_page."");
                        ?>

                        <table class="table table-bordered table-responsive" id="mytable">
                           <thead>
                             <th><input type="checkbox" id="checkall" /></th>
                             <th>Username</th>
                             <th>Email</th>
                             <th>Contact</th>
                             <th>Address</th>
                             <th>Edit</th>
                             <th>Delete</th>
                           </thead>
                           <tbody>
                           <?php 
                            foreach($tableData as $value){   
                           ?>
                             <tr>
                                <td><input type="checkbox" name="id[]" class="toedit" value="<?php echo $value['username']; ?>"/></td>
                                <td><?php echo $value['username']; ?></td> 
                                <td><?php echo $value['email']; ?></td>
                                <td><?php echo $value['phone']; ?></td>
                                <td><?php echo $value['address']; ?></td>
                                <td>
                                  <p data-placement="top" data-toggle="tooltip" title="Edit">
                                    <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit">
                                      <span class="glyphicon glyphicon-pencil"></span>
                                    </button>
                                  </p>
                                </td>
                                <td>
                                  <p data-placement="top" data-toggle="tooltip" title="Delete">
                                    <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
                                      <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                  </p>
                                </td>
                            </tr>
                            <?php }?>   
                           </tbody>
                        </table>
                        
                        
                        
                        <footer>
                          <ul class="pager">
                          
                          <?php
                          if($page == 1){
                              echo "<li class='previous disabled'><a href='#'>&larr; First page</a></li> "; 
                          }else{
                              echo "<li class='previous'><a href='admin_cust.php?&page=1'>&larr; First page</a></li>"; 
                          }
              
                          if($page != 1) {
                            $back = $page-1;
                            echo "<li class='previous'><a href='admin_cust.php?&page=".$back."'>&larr; Prev</a></li>";				
                          }else{
                            echo "<li class='previous disabled'><a href='#'>&larr; Prev</a></li>";
                          }
                          ?>
                          
                          <?php
                          echo "<h4 style='display:inline-block;'>$page </h4> <b>of</b> <h3 style='display:inline-block;'> $total_pages</h3>"; 
                          ?> 
                          
                          <?php   
                          if(($page != 1) && ($page != $total_pages))	{
                              echo "  ";
                          }
                                                                                      
                          if($page == $total_pages){
                              echo "<li class='next disabled'><a href='#'>Last page &rarr;</a></li>"; 
                          }else{
                              echo "<li class='next'><a href='admin_cust.php?&page=$total_pages'>Last page &rarr;</a></li>"; 
                          }
                          
                          if($page != $total_pages) {
                              $next = $page+1;
                              echo "<li class='next'><a href='admin_cust.php?&page=".$next."'>Next &rarr;</a></li>";
                          }else{
                              echo "<li class='next disabled'><a href='#'>Next &rarr;</a></li>";
                          }
                         ?>
                         </ul>
                       </footer>  
                    </div><!-- end of contain -->
                                        
            </div>
		</div>
	</div>
</div>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.min.js"></script>
<script>
  $(document).ready(function(){
	  	$("#checkall").click(function(){$('input:checkbox').not(this).prop('checked', this.checked);});
		$("#delete").on('click', function () {
			
			var ids = [];
			$(".toedit").each(function () {
				if ($(this).is(":checked")) {
					ids.push($(this).val());
				}
			});
			
			if (ids.length) {
				
				$.ajax({
					type: 'POST',
					url: "admin_cust_del.php",
					data: {username: ids},
					success: function (data) {
						alert(data);
						window.location.href = "admin_cust.php"; 
					}
				});
				
			} else {
				alert("Please select items.");
			}
		});

  });
</script>

</body>
</html>
