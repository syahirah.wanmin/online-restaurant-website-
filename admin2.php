<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="sumit kumar">
  <title>test</title>
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <?php 
  	include('connect.php'); 
	include('bypass.php');
	
	$customer = $db_handle->numRows("SELECT DISTINCT * FROM customer");
	$undelivered = $db_handle->numRows("SELECT * FROM `order` WHERE `order`.`delivery_status`='Pending'");
	$delivered = $db_handle->numRows("SELECT * FROM `order` WHERE `order`.`delivery_status`='Delivery'");
  
  
  ?>
 </head>
 <style>
 	.help-inline-error{color:red;}
	body {
		background-color: #f1f1f1;
	}
	body::-webkit-scrollbar {
		width: 7px;
		background-color: #084951;
	}
	body::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
	}
	body::-webkit-scrollbar-thumb {
		background-color: #FFB800;
		outline: 1px solid red;
	}
	
	.color-blue{color:#1D92AF}
	.color-segreen{color:#3F7577}
	.color-orang{color:#CE7B11}
	
	.padding-top > .col-md-6{padding-bottom:15px;}
	.wrapper {
		min-height: 2000px;
	}
	.padding-top {
		padding: 0px;
		padding-top: 15px;
		!important;
	}
	.no-margin {
		margin: 0px !important;
	}
	.no-margin {
		margin: 0px !important;
	}
	/*Top Navbar/////////START///////////*/
	
	.top-bar {
		background-color: #084951;
		padding: 0px;
		border: solid 0px;
		border-radius: 0px;
		margin-bottom: 0px;
	}
	.menu-icon {
		float: left;
		height: 50px;
		padding: 10px 15px;
		font-size: 18px;
		font-size: 25px;
		color: #fff;
		cursor: pointer;
	}
	.navbar-brand {
		float: left;
		height: 50px;
		padding: 8px 15px;
		font-size: 18px;
		line-height: 20px;
	}
	.top-bar .form-control {
		border-color: #fff;
		color: #fff;
		background-color: #000;
		border-radius: 0px;
	}
	.top-bar .navbar-form {
		padding: 0px 15px;
		margin-top: 8px;
		margin-right: -15px;
		margin-bottom: 8px;
		margin-left: -15px;
		border-top: 0px solid transparent;
		border-bottom: 0px solid transparent;
		box-shadow: none;
	}
	.navbar-nav {
		text-align: center;
	}
	.top-bar .btn-default {
		border-color: #fff;
		color: #fff;
		background-color: #000;
		border-radius: 0px;
	}
	.top-bar .badge {
		display: inline-block;
		min-width: 10px;
		position: absolute;
		top: 9px;
		padding: 2px 5px;
		font-size: 12px;
		font-weight: bold;
		line-height: 1;
		color: #fff;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		background-color: #FF5722;
		border-radius: 10px;
	}
	.top-bar .user-profile {
		height: 30px;
		width: 30px;
		border-radius: 100%;
		display: block;
		padding: 11px 0px;
	}
	.top-bar .user-profile img {
		border-radius: 100px;
	}
	.navbar-inverse .navbar-nav > li > a {
		color: #fff;
	}
	@media (max-width: 768px) {
		.nav > li {
			position: relative;
			display: inline-block;
		}
	}
	/*************begin SIDE NAV *************/
	
	
	.left-sidebar {
		position: fixed;
		left: 0px;
		top:50px;
		width: 240px;
		height: 100%;
		overflow: hidden;
		-moz-box-shadow: 1px 4px 5px 4px rgba(0,
		0,
		0,
		0.3);
		-webkit-box-shadow: 1px 4px 5px 4px rgba(0,
		0,
		0,
		0.3);
		box-shadow: 1px 4px 5px 4px rgba(0,
		0,
		0,
		0.3);
		overflow-y: scroll;
		transition: 0.5s;
		z-index: 99;
	}
	.hide-sidebar {
		position: fixed;
		left: -250px;
		top: 50px;
		width: 240px;
		height: 100%;
		overflow: hidden;
		-moz-box-shadow: 1px 4px 5px 4px rgba(0, 0, 0, 0.3);
		-webkit-box-shadow: 1px 4px 5px 4px rgba(0, 0, 0, 0.3);
		box-shadow: 1px 4px 5px 4px rgba(0, 0, 0, 0.3);
		overflow-y: scroll;
		z-index:99;
	}
	.left-sidebar::-webkit-scrollbar {
		width: 6px;
		background-color: #ECECEC;
	}
	.left-sidebar::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
	}
	.left-sidebar::-webkit-scrollbar-thumb {
		background-color: #084951;
		outline: 1px solid black;
	}
	.left-sidebar .side-nav {
		margin: 0px;
		padding: 0px;
	}
	.left-sidebar .side-nav li {
		width: 100%;
		list-style-type: none;
		padding: 0px;
	}
	.left-sidebar .side-nav li a {
		display: block;
		padding: 15px;
		text-decoration: none;
	}
	.panel {
		margin-bottom: 0px;
		background-color: #ECECEC;
		border-top: 1px solid #777;
		border-radius: 0px;
		-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
		box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
	}
	.panel a {
		color: #000;
	}
	.panel a:hover {
		background-color: #f3f3f3;
	}
	
	
	
	
	
	/*right-container*****************/
	@media (min-width: 768px) {
	
	.left-container{width:18%;float: left;transition:0.5s;}
	.right-container{width:82%;float:right;transition:0.5s;margin-top: 70px;}
	.less-width{width:0%;transition:0.5s;}
	.full-width{width:100%;transition:0.5s;}
	} 
	.mini-stat h5{
		float: left;
		margin: 0;
		text-align: left;
		font-size: 0.85em;
		color: #888888;
	}
	.mini-stat li{
		border-left: 1px solid #ddd;   
		padding-left: 10px;
		padding-right: 10px;} 
	
	.mini-stat>li:first-child {
		border-left: none;
	}
	
	.mini-stat h5 .stat-value{display:block;font-size:18px;margin-top:4px;}
	
	.right-container .breadcrumb {
		padding: 8px 15px;
		margin-bottom: 20px;
		list-style: none;
		background-color:transparent;
		border-radius: 4px;
	}
	
	.main-header {
		margin-bottom: 30px;
	}
	
	.main-header h2 {
		display: inline-block;
		vertical-align: middle;
		border-right: 1px solid #ccc;
		margin: 0;
		padding-right: 10px;
		margin-right: 10px;
	}
	
	.main-header em {
		color: #bbbbbb;
	} 
	
	.frm {
	  height: 470px; 
	  overflow-y: scroll;
	}
 </style>
<body>
  <nav class="navbar navbar-inverse top-bar navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button> <span class="menu-icon" id="menu-icon"><i class="glyphicon glyphicon-times" aria-hidden="true" id="chang-menu-icon"></i></span>
        <a class="navbar-brand" href="#"><p style="font-weight:bold; color:white; margin-top:7px;">Rudolph Food</p></a>
      </div>
      <div class="collapse navbar-collapse navbar-right" id="myNavbar">
        <ul class="nav navbar-nav">
          <li class=""><a href="#"><i class="glyphicon glyphicon-envelope"></i> <span class="badge">5</span></a> </li>
          <li class=""><a href="#" data-toggle="modal" data-target="#termsModal"><i class="glyphicon glyphicon-bell"></i> <?php if($undelivered > 0){echo "<span class='badge'>".$undelivered."</span>";}else{echo "";} ?></a> </li>
          <li class="">
            <a href="#" class="user-profile"> <span class=""><img class="img-responsive" src="photos/ProfilePic/default-avatar.jpg"></span> </a>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $_SESSION['username']; ?>          
           <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li> <a href="#"><i class="glyphicon glyphicon-user"></i> Profile</a> </li>
              <li> <a href="#"><i class="glyphicon glyphicon-cog"></i> Setting</a> </li>
              <li> <a href="index.php"><i class="glyphicon glyphicon-power-off"></i> Logout</a> </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--    top nav end===========-->
  <div class="wrapper" id="wrapper">
    <div class="left-container" id="left-container">
      <div class="left-sidebar" id="show-nav">
        <ul id="side" class="side-nav">
          <li class="panel">
            <a href="admin.php"> 
            	<i class="glyphicon glyphicon-dashboard"></i> Dashboard  
            </a>
          </li>
          <li class="panel">
            <a href="admin1.php"> 
            	<i class="glyphicon glyphicon-user"></i> User  
            </a>
          </li>
          <li class="panel">
            <a href="admin2.php"> 
            	<i class="glyphicon glyphicon-cutlery"></i> Menu  
            </a>
          </li>
          <li class="panel">
            <a href="admin3.php"> 
            	<i class="glyphicon glyphicon-bullhorn"></i> Advertisement  
            </a>
          </li>
        </ul>
      </div>
      <!-- END SIDE NAV USER PANEL -->
    </div>
    <div class="right-container" id="right-container">
        <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
            <ul class="breadcrumb">
				<li><i class="glyphicon glyphicon-home"></i><a href="#"> Home</a></li>
				<li class="active">Menu</li>
			</ul>
            </div>
            <div class="col-md-8">
            <ul class="list-inline pull-right mini-stat">
                <li>
                	<h5>CUSTOMERS <span class="stat-value color-orang"><i class="glyphicon glyphicon-plus-circle"></i> <?php echo $customer; ?></span></h5>
                </li>
                <li>
                    <h5>UNDELIVERED <span class="stat-value color-green"><i class="glyphicon glyphicon-plus-circle"></i> <?php echo $undelivered; ?></span></h5>
                </li>
                <li>
                    <h5>DELIVERED <span class="stat-value color-blue"><i class="glyphicon glyphicon-plus-circle"></i> <?php echo $delivered; ?></span></h5>
                </li>
            </ul>
            </div>
            </div>
            
            <div class="row">
            <div class="col-md-12">
                <div class="main-header">
					<h2>MENU SETTING</h2>
					<em>the first priority information</em>
				</div>
                </div>
                </div>
            
                <div class="row padding-top">
                    <div class="col-xs-12" style="margin-bottom:10px;"><button class="btn btn-danger" id="delete">delete</button></div>
                    <div class="col-md-6 frm" id="menu_list"></div>
                    <div class="col-md-6" style="background-color:white;">
                        <form class="form-horizontal" id="menu_insert">
                        <h3>Add New Menu</h3>
                            <div class="form-group">
                                <label class="control-label col-md-3">Menu Pic</label>
                                <div class="col-md-7">
                                    <input type="file" class="form-control" name="pic">
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" placeholder="menu name" name="name">
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-md-3">type</label>
                                <div class="col-md-7">
                                    <select class="form-control" name="type" id="foodType">	
                                        <option value="Food">Food</option>
                                        <option value="Drink">Drink</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Category</label>
                                <div class="col-md-7" id="foodCategory">
                                
                                
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="control-label col-md-3">Price</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" placeholder="menu price" name="prc">
                                </div>
                            </div>    
							 
                          <div class="form-group">
                                <label class="control-label col-md-3">Max order</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control"placeholder="maximum order" name="max">
                                </div>
                            </div>    
							 <div class="form-group">
                                <label class="control-label col-md-3">Reservation Time</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control"placeholder="reservation time" name="time">
                                </div>
                            </div>    
                            <div class="form-group">
                                <div class="col-xs-offset-2 col-xs-10">
                                    <button type="submit" class="btn btn-primary" id="btn_menu">Add Menu</button>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
        </div>
        
      <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="Terms and conditions" aria-hidden="true">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-body">
                  	<button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 style="font-weight:bold; color:#F9DA08;">New Order</h4>
                    <?php if($undelivered > 0){?>
                  	<table class="table table-bordered">
                    	<tr>
                        	<th>Order ID</th>
                            <th>Order Date</th>
                            <th width="50">Approve</th>
                            <th width="50">Detail</th>
                        </tr>
                        <?php 
							$data = $db_handle->runQuery("SELECT * FROM `order` WHERE `order`.`delivery_status`='Pending'"); 
							foreach($data as $kaka){
						?>
                        	<tr>
                            <td><?php echo $kaka['orderID']; ?></td>
                            <td><?php echo $kaka['order_date']; ?></td>
                            <td><button class="btn btn-success btn-sm tekan" data-delivery="<?php echo $kaka['orderID']; ?>">Delivery <span class="glyphicon glyphicon-ok"></span></button></td>
                            <td><button class="btn btn-primary btn-sm push" data-detail="<?php echo $kaka['orderID']; ?>">Details <span class="glyphicon glyphicon-search"></span></button></td>
                            </tr>
                        <?php }?>
                    </table>
                    <?php }else{echo "No notification";} ?>
                  </div>
              </div>
           </div>
      </div>
        
    </div>
    
  </div>
	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.validate.js"></script>
    
  <script type="text/javascript">
  
  		$(document).on("click", ".tekan", function () {
			
			var data = $(this).data('delivery');
			
			$.ajax({
				url: "admin_delivery.php",
				type: "POST",
				data: "id="+data,
				success: function(data){
					alert(data);window.location.reload(true);
				}
			});
		});
		
  		$(document).on("click", ".push", function () {
			
			var data = $(this).data('detail');
			
			$.ajax({
				url: "admin_deliveryDetail.php",
				type: "POST",
				data: "id="+data,
				success: function(data){
					alert(data);
				}
			});
		});
		
	  $(document).ready(function(){
		  
		  	$('#foodCategory').load('admin2_foodType.php');	
						  
		  	$('#menu_list').load('admin2_menu.php');
		  
			$("#checkall").click(function(){$('input:checkbox').not(this).prop('checked', this.checked);});
			
			$("#delete").on('click', function () {
				
				var ids = [];
				$(".toedit").each(function () {
					if ($(this).is(":checked")) {
						ids.push($(this).val());
					}
				});
				
				if (ids.length) {
					$.ajax({
						type: 'POST',
						url: "admin2_del.php",
						data: {code: ids},
						success: function (data) {
							alert(data);$('#menu_list').load('admin2_menu.php');
						}
					});
					
				} else {
					alert("Please select items to delete.");
				}
			});
			
						
		  jQuery().ready(function() {
			  
			  var v = jQuery("#menu_insert").validate({
				rules: {
					pic: {
					  required: true,
					  extension: "jpg,jpeg,png",
                	  filesize: 5
					},
					name:{
					  required: true,
					  number: false,
					  minlength: 5,
					  maxlength: 20
					}
				},
				messages:{
					pic: {
						required: "Please choose menu image",
					    extension: "Please choose correct format image",
                	  	filesize: "file size not more than 5MB"
					},
					name:{
						required: "Please insert menu name"	,
						maxlength: "name length not more than 20 character"
					}
				},
				errorElement: "span",
				errorClass: "help-inline-error",
			  });
		
			  $('body').on('click', '#btn_menu', function(){
				  
				  if (v.form()) {
					var formData = new FormData($(this).parents('#menu_insert')[0]);
			
					$.ajax({
						url: 'admin2_insert.php',
						type: 'POST',
						xhr: function() {
							var kk = $.ajaxSettings.xhr();
							return kk;
						},
						success: function (data) {
							alert(data);$('#menu_list').load('admin2_menu.php');
						},
						data: formData,
						cache: false,
						contentType: false,
						processData: false
					});
					return false;
				  }
			  });
			
		  });
			
			
			$("#foodType").change(function(){
				
				var type = $(this).val();
				
				$.ajax({
					url: "admin2_foodType.php",
					type: "POST",
					data: "type="+type,
					success: function(f){
						$('#foodCategory').html(f);
					}
				});
				
				
			});
	
	  });

  </script>
    
</body>

</html>
	
	



