# Online restaurant website 

1.0 Objectives 
 
•	To develop a live web-based responsive application.  
•	To design a website based on the Three-Tier Technology. Each website must have Business Logics which will be stated under 4.0 (a) and (b). 
 
 
2.0 Skills Involved 
 
•	Analytical skills - Website and Database Design  
•	Internet research skills - Finding appropriate references and links for your web pages 
•	Programming skills - HTML, HTML5, XML, CSS, JavaScript, PHP programming and DBMS using MySQL in a Windows environment.  
•	Creativity skills - Developing an interactive pages and add multimedia elements (text, graphics, animations) into the website.  
•	Presentation skills - In-class project presentation using PowerPoint and demonstrate the system. 
 
3.0 Project Requirements 
 
a)	Minimal Functional Requirements for the website 
•	Member Registration Page (e.g. Membership information)  
•	Authentication (e.g. Login-password page, session/cookies)  
•	Member Area (View, Add, Update, Delete, Searching, Calculation and Produce Report in pdf) 
•	Business Information (the information about the background of the company, contact details, etc.) 
•	Form Validation using JavaScript 
•	Formatting display using CSS 
 
b)	ONE additional function of your choice that you would like to add suitable for your project. (Bonus Mark). 
c)	Presentation (Use PowerPoint slides for your presentation) 
d)	System Requirements 
•	Web Server: Live server
•	Server-side scripting: PHP  
•	Client-side scripting : JavaScript, CSS 
•	Database Server: MySQL 

Disclaimer: 
1. This is not a totally complete codes - I just covered up until sign up/in. 
2. If you need a complete codes, simply contact me or else it's much better if you can try and do it by yourself!

